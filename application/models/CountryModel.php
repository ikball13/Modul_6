<?php 
class CountryModel extends CI_Model{ 
    function getCountry(){ 
        return $this->db->get("country"); 
    } 

    function insertCountry(){ 
        $country = array( 
            'Code' => $this->input->post('code'), 
            'Name' => $this->input->post('name'), 
            'Continent' => $this->input->post('continent'), 
            'Region' => $this->input->post('region'),
            'SurfaceArea' => $this->input->post('surfacearea'), 
            'IndepYear' => $this->input->post('indepyear'), 
            'Population' => $this->input->post('population'), 
            'LifeExpectancy' => $this->input->post('lifeexpectancy'),
            'GNP' => $this->input->post('gnp'), 
            'GNPOld' => $this->input->post('gnpold'),
            'LocalName' => $this->input->post('localname'),
            'GovernmentForm' => $this->input->post('governmentform'), 
            'HeadOfState' => $this->input->post('headofstate'),
            'Capital' => $this->input->post('capital'), 
            'Code2' => $this->input->post('code2')
        ); 
        return $this->db->insert('Country',$country); 
    }

    function updateCountry($code){ 
        $country = array( 
            'Code' => $this->input->post('code'), 
            'Name' => $this->input->post('name'), 
            'Continent' => $this->input->post('continent'), 
            'Region' => $this->input->post('region'),
            'SurfaceArea' => $this->input->post('surfacearea'), 
            'IndepYear' => $this->input->post('indepyear'), 
            'Population' => $this->input->post('population'), 
            'LifeExpectancy' => $this->input->post('lifeexpectancy'),
            'GNP' => $this->input->post('gnp'), 
            'GNPOld' => $this->input->post('gnpold'),
            'LocalName' => $this->input->post('localname'),
            'GovernmentForm' => $this->input->post('governmentform'), 
            'HeadOfState' => $this->input->post('headofstate'),
            'Capital' => $this->input->post('capital'), 
            'Code2' => $this->input->post('code2')
        ); 
        $this->db->where("Code",$code); 
        return $this->db->update("Country",$country); 
    }
    function getCountryByCode($code){ 
        $this->db->where("Code",$code); 
        return $this->db->get('Country'); 
    }
     function deleteCountry($code){ 
        $this->db->where('Code',$code); 
        return $this->db->delete("Country"); 
    }  
} 
?>
